

#include <iostream>       // std::cout
#include <stack>          // std::stack

int main ()
{

int myint;

  // stack::push/pop first example

  std::stack<int> myfirststack;

  std::cout << "Please enter some integers (enter 0 to end):\n";

  do {
    std::cin >> myint;
    myfirststack.push (myint);
  } while (myint);

  std::cout << "myFirstStack contains: ";

  std::cout << ' ' << myfirststack.top();

  myfirststack.pop();

  std::cout << ' ' << myfirststack.top();

  std::cout << '\n';	

  // stack::push/pop second example

  std::stack<int> mystack;

  for (int i=0; i<5; ++i) mystack.push(i);

  std::cout << "Popping out elements...";
  while (!mystack.empty())
  {
     std::cout << ' ' << mystack.top();
     mystack.pop();
  }
  std::cout << '\n';

  // stack::top

  std::stack<int> mysecondstack;

  mysecondstack.push(10);
  mysecondstack.push(20);

  mysecondstack.top() -= 5; // Manupulates the last entered object

  std::cout << "mystack.top() is now " << mysecondstack.top() << '\n';

  mysecondstack.pop();

  mysecondstack.top()-=5;

  std::cout << "mystack.top() is now " << mysecondstack.top() << '\n';

  return 0;
}

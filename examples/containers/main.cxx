#include <iostream>
#include <vector>
#include <array>
#include <map>
//#include <multimap> //No such header, included in map
#include <unordered_set>
#include <set>
int main (){
    //Vector example
    std::vector<int> myvec;
    myvec.push_back(5);
    myvec.push_back(4);
    myvec.push_back(3);
    myvec.push_back(2);
    
    for (int it = 0; it != myvec.size(); it++ ) {
        std::cout<<myvec.at(it)<<std::endl;
    }
    
    //Array example
    //Size of array must be set at compilation time
    std::array<int,4> myarray{ {5, 4, 3, 2} };
    //std::array is the same as C arrays
    int myarray2[4] = {5, 4, 3, 2};
    std::cout<<myarray.at(2)<<std::endl;
    
    //Map example
    std::map<int,char> mymap;
    mymap[1]='w';
    mymap[2]='j';
    mymap[3]='a';
    mymap[4]='k';
    std::cout << "letter mapped to key 2: "<<mymap[2]<<std::endl;
    
    //Set example
    std::set<int> myset;
    std::set<int>::iterator it;
    // set some initial values:
    for (int i=1; i<=10; i++) myset.insert(i*10);    // set: 10 20 30 40 50
    
    std::cout << "Ordered set contains:";
    for (it=myset.begin(); it!=myset.end(); ++it){
        std::cout << ' ' << *it;
    }
    std::cout << '\n';
    
    //Unordered set example
    std::unordered_set<std::string> myset2;
    std::unordered_set<std::string>::iterator it2;
    // set some initial values:
    myset2.insert("walter");
    myset2.insert("jenny");
    myset2.insert("adeel");
    
    std::cout << "Unordered set contains:";
    for (it2=myset2.begin(); it2!=myset2.end(); ++it2){
        std::cout << ' ' << *it2;
    }
    std::cout << '\n';
    
    //Multimap example
    std::multimap<int,char> mymultimap = {{1,'a'},{2,'b'},{2,'c'}};
    std::multimap<int,char>::iterator it3;
    
    //Find only spits out first element with key 2
    auto search = mymultimap.find(2);
    if (search != mymultimap.end()) {
        std::cout << "Found " << search->first << " " << search->second << '\n';
    } else {
        std::cout << "Not found\n";
    }
    
    //Finding all elements with key 2
    std::cout << "Multimap contains:";
    for (it3=mymultimap.begin(); it3!=mymultimap.end(); ++it3){
        if (it3->first==2) {
            std::cout << ' ' << it3->second;
        }
    }
    std::cout << '\n';
    
    return 0;
}
